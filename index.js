const http = require('http');

const port = 8000;
const server = http.createServer((req,res) => {
  if(req.url === '/items' && req.method === 'GET'){
    console.log(req);
    res.writeHead(200,{'Content-Type':'text/plain'})
    res.end("Data retreived from /items.")
  }
  if(req.url === '/items' && req.method === 'POST'){
    res.writeHead(200, {'Content-Type':'text/plain'})
    res.end("Data to be sent to the database!!")
  }
})
server.listen(port);
console.log(`The server connected at port ${port}`);