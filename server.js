const http = require('http');

//mock database
let directory = [
    {
        "name": "Brandon",
        "email": "brandon@email.com",
    },
    {
        "name":"Jobert",
        "email":"jobert@email.com"
    }
]
const port = 8080;

const server = http.createServer((req,res) =>{
    //Add a route here for GET method that will retrieved the content 
    //of out data base

    if(req.url === '/users' && req.method === 'GET'){
        //sets the status to 200 
        //application/json it sets the response to JSON datatype.
        res.writeHead(200, {'Content-Type': 'application/json'})
        //Input has to be data type STRIN hence the JSON.stringify(method)
        res.write(JSON.stringify(directory));
        //to end the route we add response.end
        res.end()
    } else if(req.url === '/users' && req.method === 'POST'){
        let reqBody ="";
        //to capture the input of the user and store the valuable reqBody
        req.on('data', (data)=>{
            reqBody += data
        });
        //to end the request and set the function that will be invoked right before 
        // it ends
        req.on('end',() =>{
           console.log(typeof reqBody);

           reqBody = JSON.parse(reqBody);
           console.log(typeof reqBody);
           let newUser = {
            "name" : reqBody.name,
            "email": reqBody.email
           }

           console.log(newUser);
           directory.push(newUser)
           res.writeHead(200,{'Content-Type':'application/json'})
           res.write(JSON.stringify(newUser))
           res.end()
        });
    }

   
})
server.listen(port);

console.log(`The server is running at port ${port}`)